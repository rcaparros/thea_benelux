package routines;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.TreeMap;

/*
 * user specification: the function's comment should contain keys as follows: 1. write about the function's comment.but
 * it must be before the "{talendTypes}" key.
 * 
 * 2. {talendTypes} 's value must be talend Type, it is required . its value should be one of: String, char | Character,
 * long | Long, int | Integer, boolean | Boolean, byte | Byte, Date, double | Double, float | Float, Object, short |
 * Short
 * 
 * 3. {Category} define a category for the Function. it is required. its value is user-defined .
 * 
 * 4. {param} 's format is: {param} <type>[(<default value or closed list values>)] <name>[ : <comment>]
 * 
 * <type> 's value should be one of: string, int, list, double, object, boolean, long, char, date. <name>'s value is the
 * Function's parameter name. the {param} is optional. so if you the Function without the parameters. the {param} don't
 * added. you can have many parameters for the Function.
 * 
 * 5. {example} gives a example for the Function. it is optional.
 */
public class ONE_KEY_FUNCTIONS {
	public static  java.util.TreeMap<String, String> accountsMap;
	public static  java.util.TreeMap<String, String> codesMap;
	public static  java.util.TreeMap<String, String> rtMap;
	public static  Date currentDate = TalendDate.getCurrentDate();
	
	public static void setRTMap(TreeMap<String,String> tempRTMap){
		rtMap = tempRTMap;
	}
	
	public static void setCodesMap(TreeMap<String,String> tempMap){
		codesMap = tempMap;
	}
	
	public static void setAccountMap(TreeMap<String,String> tempMap){
		accountsMap = tempMap;
	}
	
	public static String getDateFormatted(String format){
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(currentDate );		
	}
	
	public static String getDateFormatted(String format, Date dt){
		Calendar cal = Calendar.getInstance();
		cal.setTime(dt);
		cal.add(Calendar.HOUR_OF_DAY, 2);
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(cal.getTime());		
	}
	
	public static String formatText(String s){
		if(s == null || s == "")
			return "";
		else
			return s;
	}
	
	public static String encodeOneKey(String code) {
    	String value = "";
    	try{
    		if(code != null && code != ""){
		    	if(codesMap.containsKey(code))
		    		value = codesMap.get(code);
    		}
    	}catch (Exception e) {
			e.printStackTrace();
		}
    	return value;
    	
    }
	
	public static char getProcess(String process){
		if( process.equals("Creation")){
			return 'I';
		}else if(process.equals("Modification")){
			return 'U';
		}else if(process.equals("Delete")){
			return 'D';
		}else return 'I';
		
	}
	
	public static String getComments(String string1, String string2, boolean baja){
		if( string2 != null && !string2.isEmpty() && !baja){
			return string2.replace("\n", "");
		}else if( string1 != null && !string1.isEmpty()){
			return string1.replace("\n", "");
		}else{
			return "";
		}
	}
	
	public static String getComments(String string1){
		if( string1 != null && !string1.isEmpty()){
			return string1.replace("\n", "");
		}else{
			return "";
		}
	}
	
	public static String getString(String string1, String string2){
		if( string1 != null && !string1.isEmpty()){
			return string1;
		}else{
			return string2;
		}
	}
	
	public static String getCounter(Integer i){
		String number = String.valueOf(i);
		
		for(Integer j = number.length();j<10;j++){
			number = "0" + number;
		}
		
		return number;
	}
	
	public static String getFilePath(String folder, Integer counter, String type){
		return 	folder + "/" + getDateFormatted("yyyy-MM-dd-HHmmss") +  "/THEA_CEGEDIM_851_VR_" 
				+ getCounter(counter) + "_" + getDateFormatted("yyyyMMddHHmmss") + "/ES/THEA_CEGEDIM_851_VR_ES_"
				+ type + "_VALIDATION_REQUEST_" + getCounter(counter) + "_" + getDateFormatted("yyyyMMddHHmmss") + ".flat";
	}
	
	 public static String getRecordTypeWorkplace(String tipoStruc, String tipoCentro,String lab) {
		 if(tipoStruc.contains("TSR.WES.SER")){
		    return rtMap.get("Institution_Department");
		 }else{ 
		    return rtMap.get("Institution_vod");
	    }
	 }
	     
		public static String getIndividualRTId(String lab){
			if(rtMap.containsKey("TA_SP_Profesional")){
				return rtMap.get("TA_SP_Profesional");
			}
			return "";
		}
	   
		public static String workplaceName(String n){
			if(n != null && !n.isEmpty())
				return n;
			else 
				return "Sin Nombre";
		}
		public static String decodeOneKey(String code) {
	    	String value = "";
	    	try{
	    		if(code != null && code != ""){
			    	if(codesMap.containsKey(code))
			    		value = codesMap.get(code);
	    		}
	    	}catch (Exception e) {
				e.printStackTrace();
			}
	    	return value;
	    	
	    }
		
		public static Boolean addressType(String aType){
			if(aType.equals("TYS.P"))
				return true;
			else 
				return null;
		}
		
		public static Object getAddress(String externalId, java.util.TreeMap<String,Address> addressMap){
			if(addressMap.containsKey(externalId)){
				return addressMap.get(externalId);
			}
			else{
				return new Address();
			}

		}
		
}
